#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file parentesis.h
 * @brief Archivo cabecera donde se define la estructura de la clase parentesis.
 */

#include "Stack.h"

/**
 * @class parentesis
 * @brief Se declaran los métodos y atributos de la clase parentesis.
 */

class parentesis 
{
    public:

        /**
         * Constructor de la clase.
         */
        parentesis ();

        /** 
         * @brief Verifica si la palabra ingresada tiene los paréntesis balanceados.
         * @param argv Palabra que se verificará.
         */
        void balanceado ( char* argv );
};