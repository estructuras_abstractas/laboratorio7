#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file nodo.h
 * @brief Archivo cabecera donde se define la estructura de la clase nodo.
 */

/**
 * @class nodo
 * @brief Se declaran los métodos y atributos de la clase nodo.
 */

template <typename DATO>
class nodo
{
    public:

        /**
         * @brief Constructor, inicia el nodo con un dato y apunta a NULL
         * @param Info Información que se almacenará en el nodo.
         */
        nodo ( DATO info );
        
        /**
         * @brief Regresa el dato que está almacenado en el nodo.
         * @return El dato que está almacenado en el nodo.
         */
        DATO get_informacion ();

        /**
         * @brief Regresa el próximo nodo al que apunta.
         * @return El próximo nodo al que apunta.
         */
        nodo* get_ptr_siguiente_nodo ();

        /**
         * @brief Cambia la información que está almacenada en el nodo.
         * @param info La información que se almacenará.
         */
        void set_informacion ( DATO info );

        /**
         * @brief Cambia el puntero del nodo.
         * @param siguiente_nodo El nuevo nodo al que apuntará el nodo.
         */
        void set_ptr_siguiente_nodo ( nodo* siguiente_nodo );

    private:

        DATO informacion;
        nodo* ptr_siguiente_nodo;
};

template class nodo <char>;