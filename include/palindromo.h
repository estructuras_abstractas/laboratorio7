#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file palindromo.h
 * @brief Archivo cabecera donde se define la estructura de la clase palindromo.
 */

#include "Queue.h"

/**
 * @class palindromo
 * @brief Se declaran los métodos y atributos de la clase palindromo.
 */

class palindromo 
{
    public:

        /**
         * Constructor de la clase.
         */
        palindromo ();

        /**
         * @brief Verifica si una palabra es un palíndromo.
         * @param argv Palabra que se verificará.
         */
        void es_un_palindromo ( char* argv );
};