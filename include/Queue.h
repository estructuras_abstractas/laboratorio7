#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file Queue.h
 * @brief Archivo cabecera donde se define la estructura de la clase Queue.
 */

#include "nodo.h"

/**
 * @class Queue
 * @brief Se declaran los métodos y atributos de la clase Queue.
 */

template <typename DATO>
class Queue
{
    public:

        /**
         * @brief Inicializa el primer nodo de la cola a NULL.
         */
        Queue ();
        
        /**
         * @brief Inicializa el primer nodo de la cola.
         * @param informacion El dato que almacenará el primer nodo de la cola.
         */
        Queue ( DATO informacion );

        /**
         * @brief Introduce un nuevo nodo a la cola.
         * @param informacion El dato que se almacenará en la cola.
         */
        void push ( DATO informacion );

        /**
         * @brief Saca un dato de la cola.
         */
        void pop ();

        /**
         * @brief Verifica si la cola está vacía.
         * @return True si la cola está vacía, falsa si no lo está.
         */
        bool cola_vacia ();

        /**
         * @brief Imprime la información de la cola.
         */
        void imprimir_cola ();

        /**
         * @brief Regresa el último dato de la cola.
         * @return El último dato de la cola.
         */
        DATO get_info ();

    private:

        nodo<DATO>* primer_nodo;
};

template class Queue <char>; 