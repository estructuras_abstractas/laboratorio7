#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file Stack.h
 * @brief Archivo cabecera donde se define la estructura de la clase Stack.
 */

#include "nodo.h"

/**
 * @class Stack
 * @brief Se declaran los métodos y atributos de la clase Stack.
 */

template <typename DATO>
class Stack
{
    public:

        /**
         * @brief Constructor, inicializa el puntero al próximo nodo como NULL.
         */
        Stack ();

        /**
         * @brief Inicializa la información del nodo.
         * @param informacion El dato que almacenará el nodo.
         */
        Stack ( DATO informacion );

        /**
         * @brief Mete un nuevo nodo a la pila.
         * @param informacion El dato que almacenará el nuevo nodo.
         */
        void push ( DATO informacion );

        /**
         * @brief Saca el primer dato de la pila.
         */
        void pop ();

        /**
         * @brief Verifica si la pila está vacía.
         * @return Verdadero si está vacía, falso si no lo está.
         */
        bool stack_vacio ();

        /**
         * @brief Despliega en la terminal el contenido de la pila.
         */
        void imprimir_pila ();

        /**
         * @brief Regresa el contenido del primer nodo de la pila.
         * @return El primer dato de la pila.
         */
        DATO get_info ();

    private:

        nodo<DATO>* primer_nodo;
};

template class Stack <char>;