ALL: packages comp doxygen test

packages:
	@echo "**************************************************************************"
	@echo "** Se instalarán los paquetes necesarios para que funciones el programa **"
	@echo "**************************************************************************"
	@sleep 0.5s

	sudo apt install g++ -y
	sudo apt-get install build-essential -y
	sudo apt install doxygen -y
	sudo apt install doxygen-gui -y
	sudo apt-get install graphviz -y
	sudo apt install texlive-latex-extra -y
	sudo apt install texlive-lang-spanish -y
	sudo apt-get install libsfml-dev -y
	sudo apt autoremove -y


comp:
	@echo "**************************************************************************"
	@echo "***********           Se generará el ejecutable            ***************"
	@echo "**************************************************************************"
	@sleep 0.5s

	mkdir -p bin build docs

	g++ -o ./build/nodo.o -c ./src/nodo.cpp 
	g++ -o ./build/Stack.o -c ./src/Stack.cpp
	g++ -o ./build/Queue.o -c ./src/Queue.cpp
	g++ -o ./build/parentesis.o -c ./src/parentesis.cpp  
	g++ -o ./build/palindromo.o -c ./src/palindromo.cpp
	g++ -o ./build/main.o -c ./src/main.cpp 
	
	g++ -o ./bin/lab7 ./build/main.o ./build/nodo.o ./build/Stack.o ./build/Queue.o ./build/parentesis.o ./build/palindromo.o

doxygen:
	@echo "**************************************************************************"
	@echo "************      Generando el archivo refman.pdf          ***************"
	@echo "**************************************************************************"
	@sleep 0.5s

	mkdir -p docs
	doxygen Doxyfile
	make -C ./docs/latex

test:
	@echo "**************************************************************************"
	@echo "************                   Pruebas                     ***************"
	@echo "**************************************************************************"
	@sleep 0.5s

	@echo ""
	@echo "** Verificar si los paréntesis están balanceados **"
	@echo ""
	@sleep 0.5s

	@echo "-----------------------------"
	@echo " PRUEBA 1"
	@echo "-----------------------------"
	@echo " "
	./bin/lab7 parentesis asdfasdf{adaadasd
	@sleep 0.5s

	@echo "-----------------------------"
	@echo " PRUEBA 2"
	@echo "-----------------------------"
	@echo " "
	./bin/lab7 parentesis jg[adsda{sd}adads]
	@sleep 0.5s



	@echo ""
	@echo "** Verificar si una palabra es un palíndromo **"
	@echo ""
	@sleep 0.5s

	@echo "-----------------------------"
	@echo " PRUEBA 1"
	@echo "-----------------------------"
	@echo " "
	./bin/lab7 palindromo aeropuerto
	@sleep 0.5s

	@echo "-----------------------------"
	@echo " PRUEBA 2"
	@echo "-----------------------------"
	@echo " "
	./bin/lab7 palindromo reconocer
	@sleep 0.5s