# Laboratorio 7: Pilas y colas en C++


## Integrante
```
Jorge Muñoz Taylor
```

## Como compilar el proyecto
Una vez descargado el código, ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio7
```

Por último ejecute el make:
```
>>make
```
Esto creará el ejecutable, el doxygen y ejecutará las pruebas.

## Ejecutar el programa
Para ejecutar el programa debe seguir el siguiente formato:
```
./bin/labo7 parentesis/palindromo cadena
```
Donde debe escribir como primer argumento 'parentesis' si quiere verificar si la palabra tiene los paréntesis balanceados, o 'palindromo' si quiere verificar si la palabra es un palíndromo. El último argumento consiste en la cadena que quiere analizar.

**Nota importante: el paréntesis redondo ( ) deberá escribirse en la terminal con comillas simples: '(' ')' de lo contrario el programa no funcionará.

Ejemplo:

```
./bin/lab7 palindromo ana
```


## Pruebas

Para ejecutar las pruebas escriba en la terminal:
```
make test
```

## PARA Doxygen
Ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio7
```

Para compilar el doxygen:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd DOCS/latex
>>make
```
Esto generará el archivo PDF con la documentación generada por doxygen. Para verlo:
```
Doble clic al archivo refman.pdf dentro de DOCS/latex
```

## Dependencias

Debe tener instalado MAKE en la máquina, el makefile verificará e instalará todo lo siguiente:
```
>>sudo apt-get install build-essential
```
g++:
```
sudo apt install g++
```

También doxygen y latex:
```
>>sudo apt install doxygen
>>sudo apt install doxygen-gui
>>sudo apt-get install graphviz
>>sudo apt install texlive-latex-extra
>>sudo apt install texlive-lang-spanish
```