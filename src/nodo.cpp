/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file nodo.cpp
 * @brief Definen los métodos de la clase nodo.
 */ 

#include "../include/nodo.h"


template <typename DATO>
nodo<DATO>::nodo ( DATO info )
{
    this->informacion = info;
    this->ptr_siguiente_nodo = nullptr;
}


template <typename DATO>
DATO nodo<DATO>::get_informacion ()
{
    return this->informacion;
}


template <typename DATO>
nodo<DATO>* nodo<DATO>::get_ptr_siguiente_nodo ()
{
    return this->ptr_siguiente_nodo;
}


template <typename DATO>
void nodo<DATO>::set_informacion ( DATO info )
{
    this->informacion = info;
}


template <typename DATO>
void nodo<DATO>::set_ptr_siguiente_nodo ( nodo* siguiente_nodo )
{
    this->ptr_siguiente_nodo = siguiente_nodo;
}