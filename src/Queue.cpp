/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file Queue.cpp
 * @brief Definen los métodos de la clase Queue.
 */

#include <iostream>
#include "../include/Queue.h"

using namespace std;


template <typename DATO>
Queue<DATO>::Queue ()
{
    this->primer_nodo = nullptr;
}


template <typename DATO>
Queue<DATO>::Queue ( DATO informacion )
{
    this->primer_nodo = new nodo <DATO> ( informacion );
}


template <typename DATO>
void Queue<DATO>::push ( DATO informacion )
{
    //Si la cola está vacía crea el primer nodo.
    if ( this->cola_vacia() == true )
    {
        this->primer_nodo = new nodo <DATO> (informacion);//< Apunta a NULL.
        return;
    }

    nodo<DATO>* nuevo_nodo = new nodo <DATO> (informacion);
    
    nodo<DATO>* temporal = this->primer_nodo;

    //Llega hasta el último nodo.
    while (temporal->get_ptr_siguiente_nodo() != nullptr)
    {
        temporal = temporal->get_ptr_siguiente_nodo();
    }

    //Crea un nuevo nodo y hace que el nodo actual (temporal) apunte a el.
    temporal->set_ptr_siguiente_nodo ( nuevo_nodo );
}


template <typename DATO>
void Queue<DATO>::pop ()
{
    //Si la cola está vacía sale.
    if ( this->cola_vacia() == true ) return;
   
 
    //Si el primer nodo no apunta a NULL este apuntará al posterior del posterior, caso contrario el primer nodo apunta a NULL.
    if ( this->primer_nodo->get_ptr_siguiente_nodo() != nullptr )
    {
        this->primer_nodo = this->primer_nodo->get_ptr_siguiente_nodo();
    }

    else
    {
        this->primer_nodo = nullptr;
        return;     
    }

}


template <typename DATO>
bool Queue<DATO>::cola_vacia ()
{
    //Si el primer nodo apunta a NULL regresa true, en caso contrario regresa false.
    if ( this->primer_nodo == nullptr )
        return true;
    else 
        return false;
}


template <typename DATO>
void Queue<DATO>::imprimir_cola ()
{
    nodo <DATO>* temporal = this->primer_nodo;


    //Si la cola está vacía sale.
    if ( this->cola_vacia() == true ) 
    {
        cout << "Cola vacía" << endl;
        return;
    }


    //Si el primer nodo apunta a NULL, imprime su información.
    if ( temporal->get_ptr_siguiente_nodo() == nullptr )
    {
        cout << temporal->get_informacion() << endl;
        return;
    }


    //Llega hasta el último nodo.
    while ( temporal->get_ptr_siguiente_nodo() != nullptr )
    {
        cout << temporal->get_informacion() << endl;

        temporal = temporal->get_ptr_siguiente_nodo();        
    }

    cout << temporal->get_informacion() << endl;
}


template <typename DATO>
DATO Queue<DATO>::get_info ()
{
    //Si la cola está vacía regresa un espacio vacío.
    if ( this->cola_vacia() == true ) 
    {
        cout << "Cola vacía" << endl;
        return ' ';
    }

    //Regresa la información del primer nodo.
    return this->primer_nodo->get_informacion();   
}