/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file main.cpp
 * @brief Lleva toda la lógica del programa.
 */

#include <iostream>
#include <string.h>
#include "../include/nodo.h"
#include "../include/Stack.h"
#include "../include/Queue.h"
#include "../include/parentesis.h"
#include "../include/palindromo.h"

using namespace std;


int main( int argc, char** argv )
{
    parentesis parentesis_balanceados;
    palindromo Palindromo;

    //Verifica se introdujo la cantidad de argumentos adecuados y llama a los objetos para iniciar el programa.
    if( argc == 3 )
    {
        if ( strcmp(argv[1], "parentesis") == 0 )
        {
            parentesis_balanceados.balanceado ( argv[2] );
        }

        else if ( strcmp(argv[1], "palindromo") == 0 )
        {
            Palindromo.es_un_palindromo ( argv[2] );
        }
        
        else
        {
            cout << endl << "Debe escribir parentesis o palindromo seguido de una palabra :/" << endl << endl;
            return 0;
        }
            
    }
    
    else
    {
       cout << endl << "-> El formato no es el correcto, lea el README porfa  :v" << endl << endl; 
    }
    
}