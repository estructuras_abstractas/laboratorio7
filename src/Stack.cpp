/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file Stack.cpp
 * @brief Definen los métodos de la clase Stack.
 */

#include <iostream>
#include "../include/Stack.h"

using namespace std;


template <typename DATO>
Stack<DATO>::Stack ()
{
    this->primer_nodo = nullptr;
}


template <typename DATO>
Stack<DATO>::Stack ( DATO informacion )
{
    this->primer_nodo = new nodo <DATO> (informacion);
}


template <typename DATO>
void Stack<DATO>::push ( DATO informacion )
{
    //Si la pila está vacía crea un nuevo nodo y lo asigna al puntero primer_nodo.
    if ( this->stack_vacio() == true )
    {
        this->primer_nodo = new nodo <DATO> (informacion);//< Apunta a NULL.
        return;
    }

    nodo<DATO>* nuevo_nodo = new nodo <DATO> (informacion);
    
    nodo<DATO>* temporal = this->primer_nodo;

    //Recorre todos los nodos hasta llegar al último.
    while (temporal->get_ptr_siguiente_nodo() != nullptr)
    {
        temporal = temporal->get_ptr_siguiente_nodo();
    }

    temporal->set_ptr_siguiente_nodo ( nuevo_nodo );
}


template <typename DATO>
void Stack<DATO>::pop ()
{
    //Si la pila está vacía sale de la función.
    if ( this->stack_vacio() == true ) return;

    nodo<DATO>* temporal = this->primer_nodo;
    nodo<DATO>* anterior;    


    //Determina si el primer nodo apunta a null, es decir solo hay un nodo en la pila.
    if ( temporal->get_ptr_siguiente_nodo() == nullptr )
    {
        temporal ->set_ptr_siguiente_nodo(nullptr);
        this->primer_nodo = nullptr;
        return;
    }

    //Recorre todos los nodos hasta llegar al último.
    while ( temporal->get_ptr_siguiente_nodo() != nullptr )
    {
        anterior = temporal;
        temporal = temporal->get_ptr_siguiente_nodo();
    }//Fin de while

    anterior->set_ptr_siguiente_nodo(nullptr);
    
    delete (temporal);
    
    temporal = nullptr;
}


template <typename DATO>
bool Stack<DATO>::stack_vacio ()
{
    //Si el primer nodo está vacío entonces la pila lo está.
    if ( this->primer_nodo == nullptr )
        return true;
    else 
        return false;
}


template <typename DATO>
void Stack<DATO>::imprimir_pila ()
{
    nodo <DATO>* temporal = this->primer_nodo;


    //Si la pila está vacía, sale.
    if ( this->stack_vacio() == true ) 
    {
        cout << "Pila vacía" << endl;
        return;
    }


    //Si el primer nodo es NULL, imprime su dato y sale.
    if ( temporal->get_ptr_siguiente_nodo() == nullptr )
    {
        cout << temporal->get_informacion() << endl;
        return;
    }


    //Recorre todos los nodos e imprime el dato de cada uno.
    while ( temporal->get_ptr_siguiente_nodo() != nullptr )
    {
        cout << temporal->get_informacion() << endl;

        temporal = temporal->get_ptr_siguiente_nodo();        
    }

    //Imprime el dato del último nodo.
    cout << temporal->get_informacion() << endl;
}


template <typename DATO>
DATO Stack<DATO>::get_info ()
{
    nodo <DATO>* temporal = this->primer_nodo;


    //Si la pila está vacía regresa un espacio en blanco y sale.
    if ( this->stack_vacio() == true ) 
    {
        cout << "Pila vacía" << endl;
        return ' ';
    }


    //Si el primer nodo apunta a NULL imprime su dato y sale.
    if ( temporal->get_ptr_siguiente_nodo() == nullptr )
    {
        return temporal->get_informacion();
    }


    //Llega hasta el último nodo.
    while ( temporal->get_ptr_siguiente_nodo() != nullptr )
    {
        temporal = temporal->get_ptr_siguiente_nodo();        
    }

    //Imprime el dato del último nodo.
    return temporal->get_informacion(); 
}