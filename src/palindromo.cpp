/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file palindromo.h
 * @brief Definen los métodos de la clase palindromo.
 */

#include <iostream>
#include <string.h>
#include "../include/palindromo.h"

using namespace std;

palindromo::palindromo (){}


void palindromo::es_un_palindromo ( char* argv )
{
    Queue<char> cola;

    int i = 0;

    //Introduce todas las letras en la cola y cuenta el número de letras.
    while ( argv[i] != '\0' )
    {    
        cola.push ( argv[i] );
        i++;
    }


    //Recorre la palabra en sentido contrario y compara las letras con la cola.
    for ( int j=0; j<i; j++ )
    {
        if ( argv[ strlen(argv)-j-1 ] == cola.get_info() )
            cola.pop();
        
        else 
        {
            cout << endl << "-> No es un palíndromo  :(" << endl << endl;
            return;
        }

    }//Fin de for

    cout << endl << "-> Sí es un palíndromo :D" << endl << endl;
}