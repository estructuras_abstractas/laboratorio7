/**
 * @author Jorge Muñoz Taylor
 * @date 15/02/2020
 * 
 * @file parentesis.h
 * @brief Definen los métodos de la clase parentesis.
 */

#include <iostream>
#include "../include/parentesis.h"

using namespace std;

parentesis::parentesis(){}

void parentesis::balanceado ( char* argv )
{
    Stack<char> pila;

    int i = 0;

    while ( argv[i] != '\0' )
    {
        if ( pila.stack_vacio() == true )
        {
            //Verifica si los primeros paréntesis son de cierre, en este caso ya no estarán balanceados.
            if ( 
                argv[i] == ')' ||
                argv[i] == '}' ||
                argv[i] == ']'
            )  
            {
                cout << endl << "-> Paréntesis no balanceados." << endl << endl;
                return;
            }          
        }

        //Verifica si los primeros paréntesis son de apertura, en este caso se sigue con normalidad.
        if ( 
            argv[i] == '(' ||
            argv[i] == '{' ||
            argv[i] == '['
         )
         {
            //Mete el paréntesis en la pila. 
            pila.push( argv[i] );
         }


        //Se determinará si en la pila está el paréntesis antagonista, si lo está se saca de la pila
        //y se continúa, en caso contrario termina el programa e imprimirá que la palabra no tiene paréntesis balanceados.
        else if ( argv[i] == ')' )
        {
            if ( pila.get_info() == '(' )
                pila.pop();

            else
            {
                cout << endl << "-> Paréntesis no balanceados." << endl << endl;
                return;
            }
        }

        else if ( argv[i] == '}' )
        {
            if ( pila.get_info() == '{' )
                pila.pop();
            
            else
            {
                cout << endl << "-> Paréntesis no balanceados." << endl << endl;
                return;
            }
        }

        else if ( argv[i] == ']' )
        {
            if ( pila.get_info() == '[' )
                pila.pop();

            else
            {
                cout << endl << "-> Paréntesis no balanceados." << endl << endl;
                return;
            }
            
        }

        i++;
    }//Fin de while


    //Si se llegó hasta este punto y la pila está vacía, entonces la palabra tiene paréntesis balanceados.
    if ( pila.stack_vacio( ) == true )
        cout << endl << "-> Paréntesis balanceados :)" << endl << endl;

    else 
        cout << endl << "-> Paréntesis no balanceados." << endl << endl;
}